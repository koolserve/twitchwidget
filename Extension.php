<?php

namespace Bolt\Extension\koolserve\TwitchWidget;

use Bolt\Application;
use Bolt\BaseExtension;

class Extension extends BaseExtension
{
    public $configFilename = 'TwitchWidget.koolserve.yml';
    public $assetsPath = 'extensions/local/koolserve/TwitchWidget/assets/';
    
    public function initialize() {
        $this->loadConfig();
        
        if ($this->getConfigValue('enableExtension', true) !== true) {
            return false;
        } elseif($this->getConfigValue('enableDeveloperMode') === true) {
            $this->showDebugOutput();
        }
        
        $this->loadAssets();
    }

    public function getName()
    {
        return "TwitchWidget";
    }
    
    public function loadConfig()
    {
        $this->configDirectory = $this->app['resources']->getPath('config') . '/extensions';
        $this->config = $this->getConfig($configFilename);
    }
    
    public function loadAssets()
    {
        $this->addCss($this->assetsPath . 'CSS/TwitchWidget.min.css');
        
        if ($this->getConfigValue('enableDeveloperMode') == true) {
            $this->addCss('assets/css/TwitchWidget.min.css');
        } else {
            $this->addCss('assets/css/TwitchWidget.css');
        }
    }
    
    private function getConfigValue($Key, $Required = false)
    {
        if (isset($this->config[$Key])) {
            return $this->config[$Key];
        }
        
        if($Required == true){
            $this->addSnippet(
                'startofhead',
                '<!-- [TwitchWidget][ERROR] "Unable to load config value " '.$Key.' -->'
            );
        }
        
        return false;
    }
    
    private function showDebugOutput()
    {
        $this->addSnippet(
            'startofbody',
            print_r($this->config)
        );
    }

}